#ifndef TLVWORKER_H
#define TLVWORKER_H

#include <QByteArray>

enum ERROR_TYPE{
    ERROR_OK = 0,

    ERROR_UNKNOWN,
    ERROR_INCORRECT,
};

enum SIGNAL_TYPE{
    SIGNAL_ON    = 12,
    SIGNAL_OFF   = 13,
    SIGNAL_COLOR = 20,

    SIGNAL_UNKNOWN,
};

class TLVWorker
{
private:
    QByteArray a_raw;

    SIGNAL_TYPE e_type;
    int n_length;
    QByteArray a_value;

    ERROR_TYPE  checkSignal  (                ) const;
    SIGNAL_TYPE getTypeByChar(char c1, char c2) const;

    ERROR_TYPE  initFromRaw  (QByteArray   cmd);

public:
    SIGNAL_TYPE getType      () const {return e_type;}
    int         getLength    () const {return n_length;}
    QByteArray  getValue     () const {return a_value;}

    SIGNAL_TYPE handleCmd    (QByteArray cmd);
};

#endif // TLVWORKER_H
