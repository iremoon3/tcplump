#ifndef LAMPLOGIC_H
#define LAMPLOGIC_H

#include <QObject>
#include "tlvworker.h"

enum LAMP_STATE{
    LAMP_ON,
    LAMP_OFF,
    LAMP_COLOR,
};

class LampLogic: public QObject
{
    Q_OBJECT
public:
    QByteArray getColor() const {return a_color;}

public slots:
    void handleMsg(QByteArray cmd);

    LAMP_STATE getState() const       {return e_state;}

signals:
    void lampOn();
    void lampOff();
    void lampColor();

    void incorrectCommand();
    void incorrectLogic();

private:
    void setColors(QByteArray a_value)  {a_color = a_value;}
    void setState (LAMP_STATE _state)   {e_state = _state;}

    LAMP_STATE e_state = LAMP_OFF;

    TLVWorker m_tlv;

    QByteArray a_color;
};

#endif // LAMPLOGIC_H
