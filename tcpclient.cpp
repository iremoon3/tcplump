#include <QTextCodec>
#include <QStringList>
#include "tcpclient.h"

bool TCPClient::startWork()
{
    if (s_host.isEmpty() || n_port == 0)
        return false;
  
    n_nextBlockSize = 0;
  
    p_TcpSocket = new QTcpSocket(this);

    p_TcpSocket->connectToHost(s_host, n_port);
    connect(p_TcpSocket, SIGNAL(connected()), SLOT(slotConnected()));
    connect(p_TcpSocket, SIGNAL(readyRead()), SLOT(slotReadyRead()));
    connect(p_TcpSocket, SIGNAL(error(QAbstractSocket::SocketError)),
            this,       SLOT(slotError(QAbstractSocket::SocketError)));
  
    return true;
}

void TCPClient::slotReadyRead()
{
    qDebug()<<"in slotReadyRead";
    QByteArray tmp = p_TcpSocket->readAll();
    qDebug() << QString(tmp.toHex());
    emit gotMsg(tmp.toHex());
}

void TCPClient::slotError(QAbstractSocket::SocketError err)
{
    QString strError =
        "Error: " + (err == QAbstractSocket::HostNotFoundError ?
                     "The host was not found." :
                     err == QAbstractSocket::RemoteHostClosedError ?
                     "The remote host is closed." :
                     err == QAbstractSocket::ConnectionRefusedError ?
                     "The connection was refused." :
                     QString(p_TcpSocket->errorString())
                    );

    qDebug()<<strError;
}

void TCPClient::slotConnected()
{
    qDebug()<<"Received the connected() signal";
}

void TCPClient::dropSock()
{
    p_TcpSocket->disconnectFromHost();
}
