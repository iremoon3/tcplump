#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMovie>
#include "tcpclient.h"
#include "lamplogic.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void toLog(QString str);

private slots:
    void on_pushButton_released();

    void on_lampOn();
    void on_lampOff();
    void on_lampColor();

    void nextFrame();
    void logIncorrectCommand();
    void logIncorrectLogic();

private:
    void initParamsFromForms();

    Ui::MainWindow *ui;
    TCPClient m_tcpClient;
    LampLogic m_lamp;

    QTimer * p_timer;

    bool b_working = false;

    QMovie m_movie;
    double m;
    QRgb a_color;

    QString s_log;

    const QString s_namePic = ":/ico/loader.gif";
};
#endif // MAINWINDOW_H
