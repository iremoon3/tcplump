#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QCoreApplication>
#include <QPainter>
#include <QTimer>
#include <iostream>
#include <cmath>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this); 
               
    connect(&m_tcpClient, SIGNAL(gotMsg(QByteArray)),    &m_lamp, SLOT(handleMsg(QByteArray)));

    connect(&m_lamp,      &LampLogic::incorrectCommand,  this, &MainWindow::logIncorrectCommand);
    connect(&m_lamp,      &LampLogic::incorrectLogic,    this, &MainWindow::logIncorrectLogic);

    connect(&m_lamp,      &LampLogic::lampOn,            this, &MainWindow::on_lampOn);
    connect(&m_lamp,      &LampLogic::lampOff,           this, &MainWindow::on_lampOff);
    connect(&m_lamp,      &LampLogic::lampColor,         this, &MainWindow::on_lampColor);

    QPixmap pix(s_namePic);
    m = double(pix.width())/pix.height();
    m_movie.setFileName(s_namePic);
    ui->animaLabel->setMovie(&m_movie);

    p_timer = new QTimer();
    connect(p_timer, SIGNAL(timeout()), this, SLOT(nextFrame()));

    a_color = 0x66222222;
    nextFrame();
    a_color = 0xffffffff;
}

void MainWindow::nextFrame()
{
    m_movie.jumpToNextFrame();
    QPixmap  newImage = m_movie.currentPixmap();
    QPainter painter(&newImage);
    painter.setCompositionMode(QPainter::CompositionMode_Multiply);
    painter.fillRect(newImage.rect(), QRgb(a_color));
    painter.end();

    ui->animaLabel->setPixmap(newImage);
}

void MainWindow::on_lampOn()
{
    toLog("Command m_lamp on");

    p_timer->start(100);
}

void MainWindow::on_lampOff()
{
    toLog("Command m_lamp off");

    p_timer->stop();
}

void MainWindow::on_lampColor()
{
    toLog("Command m_lamp a_color");

    QString tmp("#FF");
    tmp += m_lamp.getColor().data();
    a_color = QRgb(tmp.toUtf8().constData());
}

MainWindow::~MainWindow()
{
    delete ui;
    delete p_timer;
}

void MainWindow::logIncorrectCommand()
{
    toLog("Got incorrect message");
}

void MainWindow::logIncorrectLogic()
{
    toLog("You forgot to turn on the lump");
}

void MainWindow::toLog(QString str)
{
    QDateTime date = QDateTime::currentDateTime();
    QString formattedTime = date.toString("dd.MM.yyyy hh:mm:ss");
    QByteArray formattedTimeMsg = formattedTime.toLocal8Bit();

    s_log.append(formattedTimeMsg + ": " + str +"!\n");
    ui->textEdit->setText(s_log);
}

void MainWindow::on_pushButton_released()
{
    if(!b_working){
        toLog("Try to start working");

        initParamsFromForms();

        if(m_tcpClient.startWork())
        {
            toLog("Started work");

            b_working = true;
            ui->pushButton->setText("Disconnect");
        }
    }
    else
    {
        b_working = false;
        m_tcpClient.dropSock();
        ui->pushButton->setText("Connect");

        toLog("Disconnected");
    }
}

void MainWindow::initParamsFromForms()
{
    m_tcpClient.setIP(ui->lineEdit_IP->text());
    bool ok;
    quint16 port = ui->lineEdit_Port->text().toUShort ( &ok, 10 );
    m_tcpClient.setPort(port);
}
