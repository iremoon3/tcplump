#include "tlvworker.h"
//#include <stdlib.h>
#include <QString>
#include <QtDebug>

ERROR_TYPE TLVWorker::initFromRaw(QByteArray cmd)
{
    a_raw = cmd;

    const char * cstr = a_raw.constData();
    e_type = getTypeByChar(cstr[0], cstr[1]);


    QString qstr;
    qstr.append(cstr[4]);
    qstr.append(cstr[5]);
    n_length = qstr.QString::toInt();

    a_value = QByteArray(a_raw.data() + 6, n_length + 5);

    qDebug()<< "Type " << e_type << " len " << n_length << " a_value " << a_value.data();

    return ERROR_OK;
}

SIGNAL_TYPE TLVWorker::getTypeByChar(char c1, char c2) const
{
    QString qstr;
    qstr.append(c1);
    qstr.append(c2);

    return static_cast<SIGNAL_TYPE>(qstr.QString::toInt());
}

ERROR_TYPE TLVWorker::checkSignal() const
{
   switch(e_type){

    case SIGNAL_ON:
       if(n_length == 0)
           return ERROR_OK;
       else
           return ERROR_INCORRECT;
       break;
    case SIGNAL_OFF:
       if(n_length == 0)
           return ERROR_OK;
       else
           return ERROR_INCORRECT;
       break;
    case SIGNAL_COLOR:
       if(n_length == 3)
           return ERROR_OK;
       else
           return ERROR_INCORRECT;
       break;
   }
   return ERROR_INCORRECT;
}

SIGNAL_TYPE TLVWorker::handleCmd(QByteArray cmd){
    if(initFromRaw(cmd) == ERROR_OK){
        if(checkSignal() == ERROR_INCORRECT)
            return SIGNAL_UNKNOWN;
        return getType();
    }
    else
        return SIGNAL_UNKNOWN;
}
