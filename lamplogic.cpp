#include <QtDebug>
#include "lamplogic.h"

void LampLogic::handleMsg(QByteArray cmd)
{
    SIGNAL_TYPE signal = m_tlv.handleCmd(cmd);

    qDebug() << "handleMsg " << signal;

    if(signal != SIGNAL_UNKNOWN){
        if (getState() == LAMP_OFF && signal == SIGNAL_ON){
            setState(LAMP_ON);
            emit lampOn();
            return;
        }
        else if (getState() != LAMP_OFF && signal == SIGNAL_OFF){
            setState(LAMP_OFF);
            emit lampOff();
            return;
        }
        else if (getState() != LAMP_OFF && signal == SIGNAL_COLOR){
            setState(LAMP_COLOR);
            setColors(m_tlv.getValue());
            emit lampColor();
            return;
        }
        else if (getState() == LAMP_OFF && signal == SIGNAL_COLOR){
            emit incorrectLogic();
            return;
        }
    }
    emit incorrectCommand();
}
