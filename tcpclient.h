#ifndef _MyClient_h_
#define _MyClient_h_

#include <QWidget>
#include <QTcpSocket>

class TCPClient : public QObject {
    Q_OBJECT
public:
    TCPClient(){};

    bool startWork();
    void dropSock();

    void setPort( quint16 port) { n_port = port;}
    void setIP  ( QString IP)   { s_host = IP;}

signals:
    void gotMsg(QByteArray msg);

private slots:
    void slotReadyRead   (                            );
    void slotError       (QAbstractSocket::SocketError);
    void slotConnected   (                            );

private:
    QTcpSocket* p_TcpSocket;
    quint16     n_nextBlockSize;
    QString     s_host;
    quint16     n_port;
};
#endif  //_MyClient_h_
